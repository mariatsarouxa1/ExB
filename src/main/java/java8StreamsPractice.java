import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class java8StreamsPractice {

    /*
    Stream<String[]>-> flatMap -> Stream<String>
     */
    public static void array_to_stream(){
        String[][] data = new String[][]{{"a", "b"}, {"c", "d"}, {"e", "f"}};
        List<String> lst = Stream.of(data) //Stream<String[]>
        .flatMap(x-> Arrays.stream(x)) //Stream<String>
        .map(s->s+s) //aa,bb,cc,dd,ee,ff
                .collect(Collectors.toList());
        System.out.println(lst);
    }

    public static void set_and_streams(){
        Student obj1 = new Student("mkyong");
        obj1.addBook("Java 8 in Action");
        obj1.addBook("Spring Boot in Action");
        obj1.addBook("Effective Java (2nd Edition)");

        Student obj2 = new Student("zilap");
        obj2.addBook("Learning Python, 5th Edition");
        obj2.addBook("Effective Java (2nd Edition)");

        List<Student> students = Arrays.asList(obj1,obj2);

        Stream<Set<String>> booksStream = students.stream()
                .map(student -> student.getBook());

        Stream<String> books=booksStream.flatMap(b->b.stream());

        List<String> booksList=books.collect(Collectors.toList());

        System.out.println(booksList);
    }

    public static void primitives_and_streams(){
        int[] intArray = {1, 2, 3, 4, 5, 6};

        IntStream intStream = Arrays.stream(intArray);

        Stream<int[]> arr= Stream.of(intArray);
        IntStream intStreamFromArr=arr.flatMapToInt(x -> Arrays.stream(x));

        intStreamFromArr.forEach(i -> System.out.println(i));
    }

    public static void stream_operations() {
        // 40 is the first element
        // n+2 is the operation applied on the first element
        Stream<Integer> strm = Stream.iterate(40,n->n+2);
        strm = strm.limit(20);

        IntStream instStrm = IntStream.range(1, 3);

       Stream<String> stringStream = Pattern.compile(",").splitAsStream("a,b,c");

       
    }

    public static void main(String[] args)  {
        array_to_stream();
//        set_and_streams();
//        primitives_and_streams();
    }

}

class Student {

    private String name;
    private Set<String> book;

    public Student(String name) {
        this.name=name;
    }

    public void addBook(String book) {
        if (this.book == null) {
            this.book = new HashSet<>();
        }
        this.book.add(book);
    }
    //getters and setters

    public String getName() {
        return name;
    }

    public Set<String> getBook() {
        return book;
    }
}

